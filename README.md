# Allgemeines
Die Gruppe befindet sich noch im Aufbau. Grundsätzlich sollen alle THW-Angehörige ermutig werden, sich im Bereich der Open Source Entwicklung mitzuwirken. Hierbei wird klar zwischen Entwicklung und Betrieb der Software unterschieden. Eine Entwicklung auf OpenCODE ist keine automatische Erlaubnis, den vorhandenen Quellcode im Namen der Bundesanstalt Technisches Hilfswerk zu betreiben. Alle Maintainer der Projekte können öffentlich erreichbare Betriebsplattformen einrichten, um die Software mit anderen zu testen.

# Mitwirken
## Gruppenregistrierung
Alle THW-Angehörige können sich bei dieser Gruppe registrieren. Neben der Anfrage über OpenCODE muss noch eine Anfrage an einen Maintainer mit der Übermittlung der OpenCODE-ID (z.B. @OC000012345678) erfolgen. Es wird zunächst eine Gastrolle zugewiesen. Alle weiteren Rechte werden über die Projeke vergeben.

## Vorhandene Projekte
Falls ein Projekt bereist auf einer anderen Plattform existiert, können die Maintainer via Hermine mit dem Link zum Projekt kontaktiert werden.

## Neue Projekte
Einfach die Maintainer via Hermine kontaktieren und das Projekt kurz skizzieren. Wir kümmern uns um die Organisation und Einordnung des Projekt auf der OpenCODE-Plattform.